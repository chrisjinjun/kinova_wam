#include "wam_controller_client_node.h"

WamControllerClientNode::WamControllerClientNode(){
    top_sphere_location[0]=0.5;
    top_sphere_location[1]=0.0;
    top_sphere_location[2]=0.4;

    //Client Server

    moveit_pose_client = node.serviceClient<wam_planning::IKPose>("/move_to_pose");

    // Service Servers
    wam_go_grasp_server = node.advertiseService("/wam_controller_client/wam_go_grasp", &WamControllerClientNode::wam_go_grasp, this);

    // Topic subscriptions
    poses_sub = node.subscribe("/selectedObject", 1, &WamControllerClientNode::poses_callback, this);
}

// void spinThread()
// {
// ros::spin();
// }
/*Request Wam to move*/
  bool WamControllerClientNode::wam_go_grasp(wam_controller_client::GraspOrientation::Request &goal,wam_controller_client::GraspOrientation::Response &b){
    ROS_INFO_STREAM("Setting target pose");
    update_simulation_target(goal.grasp_direction);

   ///ADD WAM POSE SERVICE 
    wam_planning::IKPose srv_pose;
    srv_pose.request.pose.position.x=target_pose.position.x;
    srv_pose.request.pose.position.y=target_pose.position.y;
    srv_pose.request.pose.position.z=target_pose.position.z;
 
    srv_pose.request.pose.orientation.x=target_pose.orientation.x;
    srv_pose.request.pose.orientation.y=target_pose.orientation.y;
    srv_pose.request.pose.orientation.z=target_pose.orientation.z;
    srv_pose.request.pose.orientation.w=target_pose.orientation.w;

    if(moveit_pose_client.call(srv_pose)) {
      ROS_INFO_STREAM("Robot send to desire pose");
    } else {
      ROS_INFO_STREAM("Failed to send robot to desire pose");
    }

    /*
    //////////////////////////////////////////////////////////////////////
 // create the action client
  // true causes the client to spin its own thread
  actionlib::SimpleActionClient<jaco_msgs::ArmPoseAction> ac("/arm_pose", true);
    // actionlib::SimpleActionClient<jaco_msgs::ArmPoseAction> ac("/arm_pose");
  //  boost::thread spin_thread(&spinThread); //NEW for multi thread

  ROS_INFO("Waiting for action server to start.");
  // wait for the action server to start
  ac.waitForServer(); //will wait for infinite time
  ROS_INFO("Action server started, sending goal.");
    jaco_msgs::ArmPoseGoal send_goal;
    //goal.header.stamp = ros::Time().now();
    //goal.goal_id.stamp = ros::Time().now();
    send_goal.pose.header.frame_id = "/jaco_api_origin";
    send_goal.pose.pose.position.x = target_pose.position.x;
    send_goal.pose.pose.position.y = target_pose.position.y;
    send_goal.pose.pose.position.z = target_pose.position.z;
    send_goal.pose.pose.orientation.x = target_pose.orientation.x;
    send_goal.pose.pose.orientation.y = target_pose.orientation.y;
    send_goal.pose.pose.orientation.z = target_pose.orientation.z;
    send_goal.pose.pose.orientation.w = target_pose.orientation.w;
  // send a goal to the action
  ac.sendGoal(send_goal);

  //wait for the action to return
  bool finished_before_timeout = ac.waitForResult(ros::Duration(7.0));

  if (finished_before_timeout)
  {
    actionlib::SimpleClientGoalState state = ac.getState();
    ROS_INFO("Action finished: %s",state.toString().c_str());
    //    spin_thread.join();
  }
  else
    {
    ROS_INFO("Action did not finish before the time out.");
    ac.cancelGoal();
    //return false;
    //   spin_thread.join();
    }

    /////////////////////////////////////////////////////////////////////////
    */
    return true;
}

/*Getting Poses from circular spheres an top sphere*/
void WamControllerClientNode::poses_callback(const pcl_interface_kinova::selectedObject::ConstPtr& selected_object)
{
  top_sphere_location[0] = selected_object->topSphere.position.x;
  top_sphere_location[1] = selected_object->topSphere.position.y;
  top_sphere_location[2] = selected_object->topSphere.position.z;

  top_sphere_orientation.x() = selected_object->topSphere.orientation.x;
  top_sphere_orientation.y() = selected_object->topSphere.orientation.y;
  top_sphere_orientation.z() = selected_object->topSphere.orientation.z;
  top_sphere_orientation.w() = selected_object->topSphere.orientation.w;

  for(int i = 0;i<8;i++) {
      circularSphere[i].point[0]=selected_object->circularSphere[i].position.x;
      circularSphere[i].point[1]=selected_object->circularSphere[i].position.y;
      circularSphere[i].point[2]=selected_object->circularSphere[i].position.z;
      circularSphere[i].orientation.x()=selected_object->circularSphere[i].orientation.x;
      circularSphere[i].orientation.y()=selected_object->circularSphere[i].orientation.y;
      circularSphere[i].orientation.z()=selected_object->circularSphere[i].orientation.z;
      circularSphere[i].orientation.w()=selected_object->circularSphere[i].orientation.w;
  }
}

/*given a grasp direction returns pose for arm*/
void WamControllerClientNode::update_simulation_target(int grasp_direction) {
  // wam_controller::Pose target_pose;

    int select_sphere = 0;
    float temp_sphere_selection = circularSphere[0].point[1];//point[0]
    //TODO: Use srv defined constants
    if(grasp_direction == TOP) {
      target_pose.position.x = top_sphere_location[0];
      target_pose.position.y = top_sphere_location[1];
      target_pose.position.z = top_sphere_location[2];
      target_pose.orientation.x =  0.0;//top_sphere_orientation.x();
      target_pose.orientation.y =  1.0;  //top_sphere_orientation.y();
      target_pose.orientation.z =  0.0; //top_sphere_orientation.z();
      target_pose.orientation.w =  0.0; //top_sphere_orientation.w();
      ROS_INFO_STREAM("Picking from TOP" << std::endl);
      ROS_INFO_STREAM(target_pose << std::endl);

    } else if(grasp_direction == LEFT) {
        ROS_INFO_STREAM("Picking from left" << std::endl);

        // for(int j = 0; j < 8 ; j++) {
        //     if(circularSphere[j].point[0] > temp_sphere_selection) {
        //         select_sphere = j;
        //         temp_sphere_selection = circularSphere[j].point[0];
        //     }
        // }

	for(int j = 0; j < 8 ; j++) {
	  if(circularSphere[j].point[1] > temp_sphere_selection) {  //Front point[1]
	    select_sphere = j;
	    temp_sphere_selection = circularSphere[j].point[1]; //.point[0]
	  }
	}

        target_pose.position.x = circularSphere[select_sphere].point[0];
        target_pose.position.y = circularSphere[select_sphere].point[1];
        target_pose.position.z = circularSphere[select_sphere].point[2];
        // target_pose.orientation.x = 0.0;//circularSphere[select_sphere].orientation.x();
	// target_pose.orientation.y = 1.0;//circularSphere[select_sphere].orientation.y();
	// target_pose.orientation.z = 0.0;//circularSphere[select_sphere].orientation.z();
	// target_pose.orientation.w = 0.0;//circularSphere[select_sphere].orientation.w();

        target_pose.orientation.x =0.0;// 0.7071067811865475;//circularSphere[select_sphere].orientation.x();
	target_pose.orientation.y = -0.707;//0.0;//circularSphere[select_sphere].orientation.y();
	target_pose.orientation.z = 0.707;//0.0;//circularSphere[select_sphere].orientation.z();
	target_pose.orientation.w = 0.0;//0.7071067811865476;//circularSphere[select_sphere].orientation.w();



	ROS_INFO_STREAM(target_pose << std::endl);
    } 


    else if(grasp_direction == RIGHT) {
        ROS_INFO_STREAM("Picking from right" << std::endl);

        for(int j = 0; j < 8; j++) {
	  if(circularSphere[j].point[1] < temp_sphere_selection) { //point[0]
                select_sphere = j;
                temp_sphere_selection=circularSphere[j].point[1];
            }
        }
        target_pose.position.x = circularSphere[select_sphere].point[0];
        target_pose.position.y = circularSphere[select_sphere].point[1];
        target_pose.position.z = circularSphere[select_sphere].point[2];

	target_pose.orientation.x = 0.0;//-0.27059805007309845;//circularSphere[select_sphere].orientation.x();
	target_pose.orientation.y = 0.707;//0.6532814824381882;//circularSphere[select_sphere].orientation.y();
	target_pose.orientation.z = 0.707;//0.2705980500730985;//circularSphere[select_sphere].orientation.z();
	target_pose.orientation.w = 0.0;//0.6532814824381883;//circularSphere[select_sphere].orientation.w();
   

        // target_pose.orientation.x = -0.707;//circularSphere[select_sphere].orientation.x();
	// target_pose.orientation.y = 0.0;//circularSphere[select_sphere].orientation.y();
	// target_pose.orientation.z = 0.0;//circularSphere[select_sphere].orientation.z();
	// target_pose.orientation.w = 0.707;//circularSphere[select_sphere].orientation.w();

    ROS_INFO_STREAM(target_pose << std::endl);

    }

    else if(grasp_direction == ROTATE) {
      target_pose.position.x = top_sphere_location[0];
      target_pose.position.y = top_sphere_location[1];
      target_pose.position.z = top_sphere_location[2];
      target_pose.orientation.x =  0.0;//top_sphere_orientation.x();
      target_pose.orientation.y =  1.0;  //top_sphere_orientation.y();
      target_pose.orientation.z =  0.0; //top_sphere_orientation.z();
      target_pose.orientation.w =  0.0; //top_sphere_orientation.w();
      ROS_INFO_STREAM("Picking from TOP" << std::endl);
      ROS_INFO_STREAM(target_pose << std::endl);      
    }

    else if(grasp_direction == PUSH_PULL) {
        ROS_INFO_STREAM("Picking from front" << std::endl);

        for(int j = 0; j < 8; j++) {
	  if(circularSphere[j].point[1] < temp_sphere_selection) { //point[0]
                select_sphere = j;
                temp_sphere_selection=circularSphere[j].point[1];
            }
        }
        target_pose.position.x = circularSphere[select_sphere].point[0];
        target_pose.position.y = circularSphere[select_sphere].point[1];
        target_pose.position.z = circularSphere[select_sphere].point[2];
	//NOTE: Check quaternion signs, remember WAM has a conjugate quaternion
	target_pose.orientation.x = 0.0;//-0.27059805007309845;//circularSphere[select_sphere].orientation.x();
	target_pose.orientation.y = 0.707;//0.6532814824381882;//circularSphere[select_sphere].orientation.y();
	target_pose.orientation.z = 0.0;//0.2705980500730985;//circularSphere[select_sphere].orientation.z();
	target_pose.orientation.w = 0.707;//0.6532814824381883;//circularSphere[select_sphere].orientation.w();
   

        // target_pose.orientation.x = -0.707;//circularSphere[select_sphere].orientation.x();
	// target_pose.orientation.y = 0.0;//circularSphere[select_sphere].orientation.y();
	// target_pose.orientation.z = 0.0;//circularSphere[select_sphere].orientation.z();
	// target_pose.orientation.w = 0.707;//circularSphere[select_sphere].orientation.w();

    ROS_INFO_STREAM(target_pose << std::endl);

    }

    if(cursor_state != TOP) {
        target_pose.position.z += 0.10;
    }
    ROS_INFO_STREAM("Target pose: " << std::endl);
    ROS_INFO_STREAM(target_pose << std::endl);

    // if(set_pose_target.call(target_pose)) {
    //     ROS_INFO_STREAM("Simulation arm target pose updated");
    // } else {
    //     ROS_INFO_STREAM("Failed to updated simulation target pose");
    // }
}


int main (int argc, char **argv)
{
  ros::init(argc, argv, "wam_controller_node");
  WamControllerClientNode wam_controller_client_node;
  ros::Rate loop_rate(10);
  ros::AsyncSpinner spinner(5);
  spinner.start();
  while (ros::ok) {
    loop_rate.sleep();
  }
  spinner.stop();

}
