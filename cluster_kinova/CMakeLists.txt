cmake_minimum_required(VERSION 2.8.3)
project(cluster_kinova)
#SET(PCL_DIR "/opt/ros/groovy/share/pcl-1.6/")
SET(PCL_DIR "/usr/share/pcl-1.7/")

find_package(PCL REQUIRED)
find_package(catkin REQUIRED COMPONENTS roscpp sensor_msgs message_generation)
#######################################
## Declare ROS messages and services ##
#######################################

#add_service_files(
#    FILES
#)

add_message_files(
    FILES
        cluster.msg
)

generate_messages(
    DEPENDENCIES
        sensor_msgs
)

###################################
## catkin specific configuration ##
###################################
catkin_package(
    CATKIN_DEPENDS
        pcl
        pcl_ros
)

###########
## Build ##
###########
include_directories(${PCL_INCLUDE_DIRS}) 
link_directories(${PCL_LIBRARY_DIRS}) 
add_definitions(${PCL_DEFINITIONS})
add_executable(cluster_kinova src/cluster_kinova.cpp)
target_link_libraries(cluster_kinova ${catkin_LIBRARIES} ${PCL_LIBRARIES})
add_dependencies(cluster_kinova cluster_kinova_gencpp)
